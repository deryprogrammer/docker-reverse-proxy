require:
* install docker
* install docker-compose
* suport linux (ubuntu,centOS,mac, etc)


Cara penggunaan


```
git clone https://gitlab.com/deryprogrammer/docker-reverse-proxy.git

```

buat hostname


```
sudo nano /etc/hosts

```

jadi seperti ini


```
127.0.0.1       localhost   microservice1.dev    microservice2.dev

```

start masing2 microservice


```

cd docker-reverse-proxy

cd microservice1

sudo docker-compose up -d

cd ../microservice2

sudo docker-compose up -d

cd ../proxy

sudo docker-compose up -d


```